/*
 * 
 * b. utilizamos el mismo metodo que en el numeral b. solo cambiamos la forma en que se reciben los datos  en el numeral a. 
 * se reciben los datos por medio de la clase Calendar y como parametro., 
 * por otro lado en este numeral recibimos los datos de el cliente con la clase Scanner. */
package Ejercicio;

import java.util.Calendar;
import java.util.Scanner;

public class P1Hora_segundos {
	public static void main(String[] args) {
		Scanner calendario = new Scanner(System.in);

		int i = 0;
		while (i < 1) {
			System.out.println("Ingrese hora, minutos y segundos:");
			int hora = calendario.nextInt(Calendar.HOUR_OF_DAY);

			if (hora > 0 && hora <24) {
				int Minutos = calendario.nextInt(Calendar.MINUTE);
				int segundos = calendario.nextInt(Calendar.SECOND);
				System.out.println("Han transcurrido " + ((hora * 3600) + (Minutos * 60) + segundos)
						+ " Segundos desde la  media noche");
				i++;
			} else {
				System.out.println("A introduccido un dato incorrecto\n");

			}
		}
	}
}

/*
 * 
 * a.
 *
 * 
 * import java.util.Calendar;
 * 
 * public class metodo { public static void main(String[] args) { Calendar
 * calendario = Calendar.getInstance(); int hora, minuto, segundo; hora =
 * calendario.get(Calendar.HOUR_OF_DAY); minuto =
 * calendario.get(Calendar.MINUTE); segundo = calendario.get(Calendar.SECOND);
 * System.out.println("Han transcurrido "+((hora*3600)+(minuto*60)+ segundo) +
 * " Segundos desde la  media noche"); } }
 * 
 */
