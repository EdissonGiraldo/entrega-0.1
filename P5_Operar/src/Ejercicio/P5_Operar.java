/*esta es la clase con la que realiza el calculo de el cuaderno
 * resta Los margenes tanto en a como en b y agrega una condicional para evitar que ingresar 
 * numero menor a los margenes y 1 cuadro.  */
package Ejercicio;

public class P5_Operar {
	public static double result;

	public static void calcularCuadro(double a1, double a2) {
		// condicion para margen
		if (a1 > 2.4 && a2 > 1.4) {
			// si se cumple realiza el calculo
			result = ((a1 - 2) / 0.5) * ((a2 - 1) / 0.5);
		}
		// si no aroja 0
		else {
			result = 00;
		}
	}

}