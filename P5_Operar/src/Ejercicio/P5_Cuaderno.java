package Ejercicio;

import Ejercicio.P5_Cuaderno;
import Ejercicio.P5_Operar;

public class P5_Cuaderno extends javax.swing.JFrame {
	// En la siguiente linea se llama a la clase P5_Operar que realiza los calculos matematicos
	P5_Operar Operar = new P5_Operar();

	public P5_Cuaderno() {
		initComponents();
	}

	@SuppressWarnings("unchecked")

	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		can1 = new javax.swing.JTextField();
		can2 = new javax.swing.JTextField();
		jButtonRes = new javax.swing.JButton();
		jLabel4 = new javax.swing.JLabel();
		jResultado = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jPanel1.setBackground(new java.awt.Color(0, 204, 204));
		jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(153, 0, 0)));

		jLabel1.setFont(new java.awt.Font("Showcard Gothic", 3, 14)); // NOI18N
		jLabel1.setForeground(new java.awt.Color(204, 0, 0));
		jLabel1.setText("Cuaderno  ¿Cuantos cuadros Tengo?");

		jLabel2.setFont(new java.awt.Font("Showcard Gothic", 3, 12)); // NOI18N
		jLabel2.setForeground(new java.awt.Color(0, 0, 153));
		jLabel2.setText("Largo de las Hojas");

		jLabel3.setFont(new java.awt.Font("Showcard Gothic", 3, 12)); // NOI18N
		jLabel3.setForeground(new java.awt.Color(0, 0, 204));
		jLabel3.setText("Ancho de las Hojas");

		can1.setFont(new java.awt.Font("Showcard Gothic", 3, 12)); // NOI18N
		can1.setForeground(new java.awt.Color(255, 0, 0));
		can1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		can1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
		can1.setSelectionColor(new java.awt.Color(0, 0, 204));
		can1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				can1ActionPerformed(evt);
			}
		});

		can2.setFont(new java.awt.Font("Showcard Gothic", 3, 12)); // NOI18N
		can2.setForeground(new java.awt.Color(255, 0, 0));
		can2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		can2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
		can2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				can2ActionPerformed(evt);
			}
		});

		jButtonRes.setBackground(new java.awt.Color(0, 51, 204));
		jButtonRes.setFont(new java.awt.Font("Showcard Gothic", 3, 12)); // NOI18N
		jButtonRes.setText("Calcular");
		jButtonRes.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
		jButtonRes.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButtonResActionPerformed(evt);
			}
		});

		jLabel4.setFont(new java.awt.Font("Showcard Gothic", 3, 14)); // NOI18N
		jLabel4.setForeground(new java.awt.Color(204, 0, 0));
		jLabel4.setText("!Tu Cuaderno Tiene ¡");

		jResultado.setFont(new java.awt.Font("Showcard Gothic", 3, 24)); // NOI18N
		jResultado.setForeground(new java.awt.Color(204, 0, 0));
		jResultado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jResultado.setText("00 Cuadros");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						jPanel1Layout.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 347,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(26, 26, 26))
				.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup().addGap(10, 10, 10).addComponent(jResultado,
								javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 152,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 152,
														javax.swing.GroupLayout.PREFERRED_SIZE))
												.addGap(43, 43, 43)
												.addGroup(jPanel1Layout
														.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(can2, javax.swing.GroupLayout.PREFERRED_SIZE, 142,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(can1, javax.swing.GroupLayout.PREFERRED_SIZE, 142,
																javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addComponent(jButtonRes, javax.swing.GroupLayout.PREFERRED_SIZE, 337,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addGap(34, 34, 34)
						.addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 66,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(38, 38, 38)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 33,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(can1, javax.swing.GroupLayout.PREFERRED_SIZE, 35,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(33, 33, 33)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 33,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(can2, javax.swing.GroupLayout.PREFERRED_SIZE, 35,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jButtonRes, javax.swing.GroupLayout.PREFERRED_SIZE, 77,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(34, 34, 34)
						.addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 55,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jResultado,
								javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(121, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE,
						369, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(0, 36, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(0, 146, Short.MAX_VALUE)));

		pack();
	}

	private void can1ActionPerformed(java.awt.event.ActionEvent evt) {
		
	}

	private void can2ActionPerformed(java.awt.event.ActionEvent evt) {
		
	}

	private void jButtonResActionPerformed(java.awt.event.ActionEvent evt) {
		// le asignamos a num1 y num2 los valores de los campos de texto can1 y can2
		double num1 = Double.parseDouble(can1.getText());
		double num2 = Double.parseDouble(can2.getText());
		//
		P5_Operar.calcularCuadro(num1, num2);
		
		jResultado.setText(String.valueOf(P5_Operar.result + " Cuadros"));
		can1.requestFocus();
		can1.selectAll();
	}

	public static void main(String args[]) {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(P5_Cuaderno.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(P5_Cuaderno.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(P5_Cuaderno.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(P5_Cuaderno.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		}
	

		/*Crea La Vista de el formulario */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new P5_Cuaderno().setVisible(true);
				
			}
		});
	}

	
	private javax.swing.JTextField can1;
	private javax.swing.JTextField can2;
	private javax.swing.JButton jButtonRes;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	public javax.swing.JPanel jPanel1;
	private javax.swing.JLabel jResultado;
}

