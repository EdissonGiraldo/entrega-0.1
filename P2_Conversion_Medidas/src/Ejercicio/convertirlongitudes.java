package Ejercicio;

import java.util.Scanner;

public class convertirlongitudes {

	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);
		// pedimos los datos al usuario
		System.out.println("Ingresa Medida A calcular");
		double metro = entrada.nextInt();
		if (metro > 0) {
			double centimetro = convertirCentimetro(metro);
			double milimetro = convertirMilimetro(metro);
			double pie = convertirPie(metro);
			double pulgada = convertirPulgada(metro);
			double yarda = convertirYarda(metro);
			double anosluz = convertirAnosluz(metro);
			double angstroms = convertirAngstroms(metro);
			// mostramos las conversiones en pantalla
			System.out.println(metro + " metros");
			System.out.println(centimetro + " cent�metros");
			System.out.println(milimetro + " mil�metros");
			System.out.println(pie + " pies");
			System.out.println(pulgada + " pulgadas");
			System.out.println(yarda + " yardas");
			System.out.println(anosluz + " anosluz");
			System.out.println(angstroms + " angstroms");
		} else {
			// en caso de ingresar un dato negativo muestra error en consola
			System.out.println("A ingresado un dato negativo");
		}
	}
// metodo para convertir metro a centrimetro
	static double convertirCentimetro(double metro) {
		double centimetro = metro * 100;
		return centimetro;
	}
	// metodo para convertir metro a milimetro
	static double convertirMilimetro(double metro) {
		double milimetro = metro * 1000;
		return milimetro;
	}
	// metodo para convertir metro a pies
	static double convertirPie(double metro) {
		double pie = metro * 3.28084;
		return pie;
	}
	// metodo para convertir metro a pulgadas
	static double convertirPulgada(double metro) {
		double pulgada = metro * 39.3701;
		return pulgada;
	}
	// metodo para convertir metro a yardas
	static double convertirYarda(double metro) {
		double yarda = metro * 1.09361;
		return yarda;
	}
	// metodo para convertir metro a a�os luz
	static double convertirAnosluz(double metro) {
		double anosluz = metro * 0.0000000000000001057;
		return anosluz;
	}
	// metodo para convertir metro a Angstroms
	static double convertirAngstroms(double metro) {
		double angstroms = metro * 10000000000.0;
		return angstroms;
	}

}
