package Ejercicio;
import java.util.Scanner;
public class P4_Ofertas {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
//Ponemos un ciclo para que el usuario ingrese correctamente el dato
		int i = 0;
		while (i < 1) {
			System.out.println("Ingrese los precios de las tiendas A,B,C:");
			double a, b, c, d;
			// crea las variables atraves de lo que ingresa el usuario
			a = entrada.nextInt();
			b = entrada.nextInt();
			c = entrada.nextInt();
			// Aca validamos que el numero no sea negativo
			if (a >= 0 && b >= 0 && c >= 0) {
				System.out.println("Ingrese el % del descuento: ");
				d = entrada.nextInt();
				double descuento = d / 100;
				double tienda1 = (a * 3) - (a * 3 * descuento);
				double tienda2 = (b * 2);
				double tienda3 = (c * 3);

				// aca ya buscamos la que mejor precio tiene
				if (tienda1 < tienda2 && tienda1 < tienda3) {
					System.out.println("La tienda 1 tiene el precio mas bajo " + tienda1);
				}
				if (tienda2 < tienda1 && tienda2 < tienda1) {
					System.out.println("La tienda 2 tiene el precio mas bajo " + tienda2);
				} else if (tienda3 > tienda1 && tienda3 < tienda2) {
					System.out.println("La tienda 3 tiene el precio mas bajo " + tienda3);
				}
				i++;
			}

			else {
				System.out.println("A ingresado un dato negativo\n");
				//En caso de que el usuario ingrese un dato negativo se le muestra el mensaje
				// en pantalla y se vuelve a iniciar el ciclo.
			}
		}
	}
}