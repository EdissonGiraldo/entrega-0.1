package Ejercicio;
public class P6_Ascii_art {
	//Creamos el metodo principal main
	public static void main(String[] args) {
		//Imprimimos los souts para el dibujo
		System.out.println("");
		System.out.println("");
		System.out.println("    $    $   $    $   $   $");
		System.out.println("    $    $   $    $   $   $");
		System.out.println("    $ $ $ $ $ $ $ $ $ $ $ $");
		System.out.println("    $  $   $   $  $   $   $");
		System.out.println("    $                     $");
		System.out.println("    $                     $");
		System.out.println("    $                     $");
		System.out.println("    $                     $");
		System.out.println("    $    #              #   $");
		System.out.println("    $   ###            ###   $");
		System.out.println("    $    #              #   $");
		System.out.println("   $                      $");
		System.out.println("  $                         $$");
		System.out.println("   $                         $$");
		System.out.println("    $                         $$");
		System.out.println("    $                        $$$$");
		System.out.println("    $              $$$$$$$$$$$");
		System.out.println("    $                    $");
		System.out.println("    $                   $");
		System.out.println("     $                 $");
		System.out.println("      $     $$$$$$$$$");
		System.out.println("      $            $");
		System.out.println("     $              $");
		System.out.println("    $                $");
		System.out.println("   $                  $");
		System.out.println("  $                    $");
		System.out.println(" $                      $");
		System.out.println("");
		System.out.println("");
	}
}