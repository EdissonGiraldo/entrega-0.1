package Ejercicio;
import java.util.Scanner;
public class P3_Temperatura {
    public static void main(String[] args) {
    Scanner entran = new Scanner(System.in);
    System.out.println("Ingresa Grados Centigrados para calcular");   
    int temperar = entran.nextInt();
    int fahrenheit = converFahrenheit (temperar);
    int kelvin = converKelvin (temperar);
    int rankine = converRankine (temperar);
    System.out.println(fahrenheit + " Fahrenheit");
    System.out.println(kelvin +" Kelvin");
    System.out.println(rankine +" Rankine");
    }
    
   
        static int converFahrenheit (int temperar){
        int fahrenheit= temperar*2-temperar/5;// aca cambia segun el calculo a realizar
        fahrenheit= fahrenheit+32;
        return fahrenheit;
        } 
        
        static int converKelvin (int temperar){
        int kelvin = temperar + 273;// aca cambia segun el calculo a realizar
        return kelvin;
        }
        
        static int converRankine (int temperar){
        int  rankine = temperar * 100;// aca cambia segun el calculo a realizar
        return  rankine;
        }
}